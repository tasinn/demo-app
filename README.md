# Demo APP

demo-app is a django app with simple auth, inventory, cart, order RESR API.

## Installation
This app is tested with python 3.8  
Install python 3.8 and virtualenv, then
follow the below procedure to run the app

```bash
virtualenv env
source env/bin/activate
python manage.py migrate
python manage.py runserver
```
## Installation with docker
If you have docker compose, then run the following command
```bash
docker-compose up
```

## Usage
API Endpoints:
Append these URI after the base url

| Endpoints                     | Method         | Usage
| -----------                   | -----------    | -----------
| /api/user/create              | POST           | creates an user
| /api/user/list                | GET            | list all available users, only admin can access
| /api/user/update              | PUT            | user updates own data
| /api/user/login               | POST           | user logs in & gets jwt 
| /api/user/details             | GET            | gets user details
| /api/inventory                | POST           | admin can add item in inventory
| /api/inventory/list           | GET            | all inventory items
| /api/inventory/search         | POST           | search an item in inventory
| /api/inventory/<inventory_id> | GET, PUT       | gets and updates an item in inventory
| /api/cart                     | GET, POST      | get all cart items and adds item to user cart
| /api/cart/<cart_id>           | GET, DELETE    | gets and deletes cart item
| /api/cart/order/<order_id>    | GET            | gets specific orders details with items
| /api/order                    | GET, POST      | gets all orders and creates order with existing cart items
| /api/order/<order_id>         | GET            | gets order summary


## Testing Task:
**To Test the app you should run the app with docker**  
An admin user is created for the test purpose. `email: admin@demoapp.com & password: admin `   
Admin should be able to do everything.  
User should be able to add or modify his/her data only.  
Create an user to test the API Set.  
You should use the admin user to add inventory and perform other admin tasks like order status change or view all lists. 
The auth token is a `jwt` token found after login and should be used with other requests except user create by adding it to the Authorization header. Sample: `Authorization: jwt <token>`

## Example Test:
This is a happy path  
* Admin logs in  
* Admin adds item to inventory  
* User logs in  
* User adds item in cart  
* User submits order  


## Sample request bodies

**/api/user/create** POST
```json
{
    "email": "any email address",
    "username": "test",
    "password": "password"
}
```

**/api/user/login** POST
```json
{
    "email": "any email address",
    "password": "password"
}
```

**/api/inventory** POST
```json
{
    "name": "Napa",
    "volume": "50mg",
    "amount": 10000,
    "price": 1.50,
    "type": "tablet"
}
```

**/api/inventory/search** POST
```json
{
    "name": "Napa"
}
```

**/api/cart** POST
```json
{
    "inventory": 1,
    "amount": 50
}
```

**/api/order** POST
```json
no request body needed
```