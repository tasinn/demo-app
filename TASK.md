# Assignment for Test Engineer Candidates:

**Follow the steps below to complete the task:**


* If you do not have docker installed on your system, then you need to install docker. docker-compose command should be available in your terminal/cmd shell. To install docker you may follow the instructions in the below links:  
	[Install on Windows](https://docs.docker.com/docker-for-windows/install/)  
    [Install on Mac](https://docs.docker.com/docker-for-mac/install/)  
    [Install on Linux](https://docs.docker.com/compose/install/)  
* Install `git` on your system. Alternatively you can download from the gitlab [repo](https://gitlab.com/tasinn/demo-app).
* Execute the below command to clone the project in your local machine  
    `git clone https://gitlab.com/tasinn/demo-app.git`
* Go to the Project root and execute the below command to run the project. Make sure the `8001` port of your machine is free. It will take long time when executing the command first time.  
    `docker-compose up`  
    If everything goes well after sometime you will notice the below logs in your terminal  
    ```
    api_1  | System check identified no issues (0 silenced).
    api_1  | June 16, 2020 - 07:17:02
    api_1  | Django version 3.0.7, using settings 'demo_app.settings'
    api_1  | Starting development server at http://0.0.0.0:8001/
    api_1  | Quit the server with CONTROL-C.
    ```  
    So the server is up and running on `8001` port. `localhost:8001` will be your base url
* Now go to the gitlab repo and read the `README.md` file. You will find the Endpoints there
* Read all the texts beside the endpoints
* An admin user is created for you. To login with the admin user you should use the following email and password.  
    ```
    email: admin@demoapp.com
    password: admin
    ```
* Now create test cases and execute the tests. You should write the test cases and test results in an excel file/google sheet with proper standards. Give the file a proper name
* The next task is to automate the test cases of basic flow of the app with any language you are good at. Your test suite should be well organized and should contain a `README.md` file. Make a public repo and upload the code there. If you are using any framework or custom framework please mention and give summary of it.
* Now share the excel file and aautomaion test suite link in email.

### Bonus
Try to find the corner cases as much as possible. There is a major flaw in the app. If you can find or mention it, then it will be a bonus for you. Also you can share your feedback to improve the app in a separate file. It will count as bonus also.