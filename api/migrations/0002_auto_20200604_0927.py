# Generated by Django 3.0.7 on 2020-06-04 09:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MedicineAddRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100)),
                ('status', models.CharField(default=None, max_length=50, null=True)),
                ('amount', models.IntegerField(default=None, null=True)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('clicked', models.BooleanField(default=False)),
                ('click_time', models.DateTimeField(default=None, null=True)),
                ('content_id', models.PositiveIntegerField()),
                ('description', models.CharField(max_length=500)),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='PrescriptionDecoded',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(default='valid', max_length=50, null=True)),
                ('amount', models.IntegerField(default=1)),
            ],
        ),
        migrations.RemoveField(
            model_name='prescriptiondecodednotfound',
            name='decoder',
        ),
        migrations.RemoveField(
            model_name='prescriptiondecodednotfound',
            name='prescription',
        ),
        migrations.RenameField(
            model_name='requestdecoded',
            old_name='requested_by',
            new_name='owner',
        ),
        migrations.RemoveField(
            model_name='cart',
            name='request_decoded',
        ),
        migrations.RemoveField(
            model_name='inventory',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='order',
            name='user_order_id',
        ),
        migrations.RemoveField(
            model_name='prescription',
            name='user_decode_request',
        ),
        migrations.RemoveField(
            model_name='requestdecoded',
            name='status',
        ),
        migrations.RemoveField(
            model_name='user',
            name='last_password_change',
        ),
        migrations.RemoveField(
            model_name='userdecoderequest',
            name='user_req_id',
        ),
        migrations.AddField(
            model_name='cart',
            name='amount',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='cart',
            name='inventory',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='api.Inventory'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='inventory',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='inventor', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='name',
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
        migrations.AddField(
            model_name='order',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='order', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='total_amount',
            field=models.FloatField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='prescription',
            name='name',
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
        migrations.AddField(
            model_name='prescription',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='prescription', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='requestdecoded',
            name='decoder',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='req_decoder', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='requestdecoded',
            name='source',
            field=models.CharField(default='prescription', max_length=20),
        ),
        migrations.AddField(
            model_name='user',
            name='address',
            field=models.CharField(default=None, max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='mobile_no',
            field=models.CharField(default=None, max_length=11, null=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='order',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cart_order', to='api.Order'),
        ),
        migrations.AlterField(
            model_name='inventory',
            name='amount',
            field=models.IntegerField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='inventory',
            name='price',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='inventory',
            name='status',
            field=models.CharField(default='available', max_length=50),
        ),
        migrations.DeleteModel(
            name='MedicineNotFound',
        ),
        migrations.DeleteModel(
            name='PrescriptionDecodedNotFound',
        ),
        migrations.AddField(
            model_name='prescriptiondecoded',
            name='decoder',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='medicine_prescription_decoder', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='prescriptiondecoded',
            name='inventory',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='prescription_inventory', to='api.Inventory'),
        ),
        migrations.AddField(
            model_name='prescriptiondecoded',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pres_decoded', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='prescriptiondecoded',
            name='prescription',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='prescription_decoded', to='api.Prescription'),
        ),
        migrations.AddField(
            model_name='notification',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notification', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='medicineaddrequest',
            name='inventory',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Inventory'),
        ),
        migrations.AddField(
            model_name='medicineaddrequest',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='med_add_request', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='medicineaddrequest',
            name='prescription',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Prescription'),
        ),
        migrations.AddField(
            model_name='medicineaddrequest',
            name='updated_by',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
