from django.db import models
import jsonfield
import datetime
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
import uuid
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, **kwargs):
        # Ensure that an email address is set
        if not kwargs.get('email'):
            raise ValueError('Users must have a valid e-mail address')

        # Ensure that a username is set
        if not kwargs.get('username'):
            raise ValueError('Users must have a valid username')

        if not kwargs.get('password'):
            raise ValueError('Users must have a password')

        username = kwargs.get('username')
        email = kwargs.get('email')
        password = kwargs.get('password')

        user = self.model(
            email = self.normalize_email(email),
            username = username
        )

        user.set_password(kwargs.get('password'))
        user.save()

        return user

    # def create_superuser(self, email, password=None, **kwargs):
    #     account = self.create_user(email, password, kwargs)

    #     account.is_admin = True
    #     account.save()

    #     return account

class User(AbstractBaseUser):
    created = models.DateTimeField(auto_now_add=True)
    username = models.CharField(max_length = 50, blank = False)
    email = models.EmailField(max_length = 50, blank = False, unique=True)
    password = models.CharField(max_length = 200, blank = False)
    role = models.CharField(max_length = 20, default = 'user')
    mobile_no = models.CharField(max_length=11, null=True, default=None)
    address = models.CharField(max_length=300, null=True, default=None)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'password']

    class Meta:
        ordering = ('created', 'username')

    def clean(self):
        if not re.match(r'01[3-9][0-9]{8}', mobile_no):
            raise ValidationError("Invalid Mobile No.")


# status = available, not_available
class Inventory(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length = 100, blank = False)
    type = models.CharField(max_length = 50, blank = False)
    volume = models.CharField(max_length = 50, blank = False)
    status = models.CharField(max_length = 50, blank = False, default = 'available')
    amount = models.IntegerField(default = None, null = True)
    price =  models.FloatField(null = False)
    owner = models.ForeignKey('User', related_name = 'inventor', on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)
        unique_together = ('name', 'volume')


# order = null, id 
class Cart(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey('User', on_delete=models.CASCADE)
    order = models.ForeignKey('Order', related_name = 'cart_order', on_delete = models.CASCADE, default = None, null = True)
    # request_decoded = models.ForeignKey('RequestDecoded', related_name = 'cart', on_delete = models.CASCADE, null = False, blank = False)
    # user_decode_request = models.ForeignKey('UserDecodeRequest', on_delete = models.CASCADE)
    amount = models.IntegerField(default=1, null=False)
    inventory = models.ForeignKey('Inventory', on_delete=models.CASCADE) 


#status = pending, processed, delivered
class Order(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    status =  models.CharField(max_length = 100, blank = False, default = 'pending')
    total_amount = models.FloatField(null = True, blank = False, default = None)
    owner = models.ForeignKey('User', related_name='order', on_delete=models.CASCADE)
    name = models.UUIDField(default = uuid.uuid4, editable=False, blank = False, null = False, unique=True)




