from rest_framework import permissions
from rest_framework_jwt.settings import api_settings
import jwt
from rest_framework.response import Response
# from rest_framework import exceptions
from api.utils import *
from django.contrib.auth.models import AnonymousUser
# from django.utils.translation import gettext as _
# import django.core.exceptions as e


jwt_decode_handler = api_settings.JWT_DECODE_HANDLER


def get_payload(request):
    try:
        token = request.META['HTTP_AUTHORIZATION']
        token = token.split(' ')[1]
    except:
        raise AuthMissing("Credintials Missing.")
    try:
        payload = jwt_decode_handler(token)
    except:
        raise InvalidAuthentication(detail = "Authentication Failed.")

    return payload



class IsAuthenticated(permissions.BasePermission):
    
    def has_permission(self, request, view):
        # print("================================")
        # token = request.META['HTTP_AUTHORIZATION']
        if request.user.is_anonymous:
           raise AuthMissing("Credintials Missing.")
        return True

class IsAdmin(permissions.BasePermission):
    
    def has_permission(self, request, view):
        return request.user.role == 'admin'
            
class IsAdminOrOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.user.role == 'admin':
            return True
        if hasattr(obj, 'owner'):
            return obj.owner == request.user
        else:
            return obj == request.user



class IsAdminOrOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.role == 'admin':
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.owner == request.user


# class IsOwnerOrReadOnly(permissions.BasePermission):
#     """
#     Custom permission to only allow owners of an object to edit it.
#     """

#     def has_object_permission(self, request, view, obj):
#         # Read permissions are allowed to any request,
#         # so we'll always allow GET, HEAD or OPTIONS requests.
#         if request.method in permissions.SAFE_METHODS:
#             return True

#         # Write permissions are only allowed to the owner of the snippet.
#         return obj.owner == request.user









