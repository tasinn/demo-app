from rest_framework import serializers
from api.models import (
                        User, 
                        Inventory,
                        Order,
                        Cart
                        )
from api.utils import InvalidAuthentication
from django.contrib.auth.hashers import make_password, check_password
import datetime
import re




class InventorySerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source = 'owner.id')
    class Meta:
        model = Inventory
        fields = ('id', 'name', 'type', 'volume', 'status', 'amount', 'price', 'owner')


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    current_password = serializers.CharField(write_only=True, required=False)
    
    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'mobile_no', 'address', 'current_password') 


    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    def validate(self, data):
        # print(re.match(r'01[5-9][0-9]{8}', data))
        if data.get('mobile_no') is not None:
            if not re.match(r'01[3-9][0-9]{8}', data['mobile_no']):
                raise serializers.ValidationError("Invalid Mobile No.")
        return data

    def update(self, instance, validated_data):
        instance.address = validated_data.get('address', instance.address)
        instance.mobile_no = validated_data.get('mobile_no', instance.mobile_no)
        instance.username = validated_data.get('username', instance.username)
        password = validated_data.get('password', None)
        if password:
            current_password = validated_data.get('current_password', None)
            if current_password is None:
                raise InvalidAuthentication(detail = 'current password needed.')
            
            if not check_password(current_password, instance.password):
                raise InvalidAuthentication(detail = 'wrong current password.')
            
            instance.set_password(password)           

        instance.save()
        return instance



class OrderSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source = 'owner.id')
    name = serializers.ReadOnlyField()
    created = serializers.ReadOnlyField()
    class Meta:
        model = Order
        fields = ('id', 'status', 'total_amount', 'owner', 'name', 'created')

class CartSerializer(serializers.ModelSerializer):
    # owner = serializers.ReadOnlyField(source = 'owner.id')
    owner = UserSerializer(read_only=True)
    order = serializers.ReadOnlyField(source = 'order.id')
    inventory = InventorySerializer(read_only=True)
    # user_decode_request = serializers.ReadOnlyField(source = 'user_decode_request.id')
    # request_decoded = serializers.ReadOnlyField(source='request_decoded.id')
    class Meta:
        model = Cart
        fields = ('id', 'owner', 'order', 'inventory', 'amount')

class InventorySearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = ('id', 'name', 'price', 'type', 'volume', 'amount', 'status')
