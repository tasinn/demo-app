from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from api import views
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    # url(r'^stayinfo$', views.StayInfoList.as_view()),
    # url(r'^stayinfo/(?P<pk>[0-9]+)$', views.StayInfoDetails.as_view()),
    url(r'^cart$', views.CartList.as_view()),
    url(r'^cart/(?P<pk>[0-9]+)$', views.CartDetails.as_view()),
    url(r'^cart/order/(?P<order>[0-9]+)$', views.CartListByOrder.as_view()),
    url(r'^order$', views.OrderList.as_view()),
    url(r'^order/(?P<pk>[0-9]+)$', views.OrderDetails.as_view()),
    url(r'^inventory$', views.InventoryCreate.as_view()),
    url(r'^inventory/list$', views.InventoryList.as_view()),
    url(r'^inventory/(?P<pk>[0-9]+)$', views.InventoryDetails.as_view()),
    url(r'^inventory/search$', views.InventorySearch.as_view()),
    url(r'^user/list$', views.UserList.as_view()),
    url(r'^user/create$', views.UserCreate.as_view()),
    url(r'^user/update', views.UserUpdate.as_view()),
    url(r'^user/details', views.UserDetails.as_view()),
    url(r'^user/login$', views.Login.as_view()),
    url(r'^api-token-auth/', obtain_jwt_token),  
]

urlpatterns = format_suffix_patterns(urlpatterns)