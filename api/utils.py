from rest_framework import exceptions
from rest_framework import status
from django.utils.translation import ugettext_lazy as _
# from rest_framework.authentication import BaseJSONWebTokenAuthentication


class AuthMissing(exceptions.APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = _('Authentication credentials were not provided.')
    default_code = 'not_authenticated'


class InvalidAuthentication(exceptions.APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = _('Incorrect authentication credentials.')
    default_code = 'authentication_failed'
    
# class Abc(BaseJSONWebTokenAuthentication):
    
