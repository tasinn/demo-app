import datetime
from api.models import (
                        User, 
                        Inventory,
                        Order,
                        Cart,
                        )
# from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework import generics
from api.serializers import (
                            UserSerializer, 
                            InventorySerializer,
                            OrderSerializer,
                            CartSerializer,
                            InventorySearchSerializer,
                            )
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.hashers import make_password, check_password
from rest_framework_jwt.settings import api_settings
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly
from api.permissions import IsAuthenticated, IsAdminOrOwnerOrReadOnly, IsAdmin, IsAdminOrOwner
from rest_framework import exceptions
import uuid

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_secret_key = api_settings.JWT_SECRET_KEY



class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [
        IsAuthenticated,
        IsAdmin
    ]
class UserCreate(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [
        AllowAny
    ]
class UserDetails(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [
        IsAuthenticated,
        IsAdminOrOwner
    ]

    def retrieve(self, request, *args, **kwargs):
        resp = {
            'username': request.user.username,
            'email': request.user.email,
            'mobile_no': request.user.mobile_no,
            'address': request.user.address
        }

        return Response(resp, status=200)

class UserUpdate(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [
        IsAuthenticated,
        IsAdminOrOwner
    ]

    def update(self, request, *args, **kwargs):
        instance = request.user
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)



class InventoryCreate(generics.CreateAPIView):
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer
    permission_classes = [
        IsAuthenticated,
        IsAdmin
    ]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class InventoryList(generics.ListAPIView):
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer
    permission_classes = [
        IsAuthenticated,
    ]



class InventoryDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer
    permission_classes = [
        IsAuthenticated,
        IsAdminOrOwnerOrReadOnly 
    ]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)
        return Response(serializer.data)






class OrderList(generics.ListCreateAPIView):
    serializer_class = OrderSerializer
    permission_classes = [
        IsAuthenticated
    ]

    def get_queryset(self):
        if self.request.user.role == 'admin':
            return Order.objects.all()
        return Order.objects.filter(owner = self.request.user.id)

    def perform_create(self, serializer):
        user = User.objects.filter(pk=self.request.user.id).first()
        if user.address is None or user.mobile_no is None:
            raise ValidationError('Please provide address and mobile no in profile.')
        serializer.save(owner = self.request.user)

    def create(self, request, *args, **kwargs):
        cart_obj = Cart.objects.filter(owner=request.user, order = None)
        if cart_obj.first() is None:
            return Response({'error': 'no item found in cart'}, status=400)
        
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
        except Exception as e:
            return Response({'error': str(e)}, status=400)

        total_price = 0
        for obj in cart_obj:
            inv = Inventory.objects.filter(pk=obj.inventory.id)
            if inv.first().amount < obj.amount:
                return Response(
                    {
                        'error': 'Sorry, your order can not be confirmed as some items are unavailable right now',
                        'status': 400
                    }
                    )
            inv.update(amount=inv.first().amount-obj.amount)
            total_price += (obj.amount * inv.first().price)

        serializer.is_valid(raise_exception=True)
        serializer.save(total_amount=total_price)
        cart_obj.update(order=serializer.data.get('id'))

        
        return Response(serializer.data, status=201)


class OrderDetails(generics.RetrieveUpdateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [
        IsAuthenticated,
        IsAdminOrOwner
    ]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user.role != 'admin':
            return Response({'error': "You are not authorized"}, status = 403)
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)
        return Response(serializer.data)
        
        


class CartListByOrder(generics.ListAPIView):
    serializer_class = CartSerializer
    permission_classes = [
        IsAuthenticated

    ]

    def get_queryset(self):
        order = self.kwargs['order']
        if self.request.user.role == 'admin':
            return Cart.objects.filter(order=order)
        return Cart.objects.filter(order=order, owner=self.request.user.id)


class CartList(generics.ListCreateAPIView):
    serializer_class = CartSerializer
    permission_classes = [
        IsAuthenticated
    ]

    def get_queryset(self):
        if self.request.user.role == 'admin':
            return Cart.objects.all()
        return Cart.objects.filter(owner=self.request.user.id, order=None)

    def perform_create(self, serializer):
        inventory_obj = Inventory.objects.filter(pk=self.request.data.get("inventory")).first()
        if inventory_obj.amount < self.request.data.get('amount'):
            raise ValidationError('Sorry, Your requested amount is not available right now')
        serializer.save(owner=self.request.user, inventory=inventory_obj)
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            self.perform_create(serializer)
        except Exception as e:
            print(e)
            return Response({
                'error': e
            },
                status=400
            )
        return Response(serializer.data, status=201)





class CartDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    permission_classes = [
        IsAuthenticated,
        IsAdminOrOwner
    ]



class InventorySearch(APIView):
    permission_classes = [
            IsAuthenticated
    ]
    def post(self, request, format=None):
        serializer = InventorySearchSerializer(data=request.data)
        serializer.is_valid()
        search_result_obj = Inventory.objects.filter(name__icontains=request.data['name'])
        search_result = InventorySearchSerializer(search_result_obj, many=True)

        if not search_result.data:
            resp = {'id': None}
            return Response(resp, status = 404)
        return Response(search_result.data, status = 200)


class Login(APIView):
    permission_classes = [
            AllowAny # Or anon users can't login
    ]
    def post(self, request, format = None):
        email = request.data['email']
        password = request.data['password']

        user = User.objects.filter(email = email)
        user = user.first()
        if user:
            if check_password(password, user.password):
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                resp = {    'email': email,
                            'message': 'logged in',
                            'token': token,
                            'role': user.role
                       }

                

                return Response(resp, status = 200)
        return Response({'message': 'wrong credintials'}, status = 401)
